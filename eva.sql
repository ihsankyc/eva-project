-- --------------------------------------------------------
-- Sunucu:                       localhost
-- Sunucu sürümü:                10.4.11-MariaDB - mariadb.org binary distribution
-- Sunucu İşletim Sistemi:       Win64
-- HeidiSQL Sürüm:               11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- tablo yapısı dökülüyor eva_exchange.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `LogID` int(11) NOT NULL AUTO_INCREMENT,
  `LogType` enum('Buy','Sell') DEFAULT NULL,
  `PortfolioID` int(11) DEFAULT NULL,
  `BuyerID` int(11) DEFAULT NULL,
  `LogDate` datetime DEFAULT NULL,
  `LogStatus` tinyint(1) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='All logs for Buy/Sell';

-- eva_exchange.logs: ~11 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` (`LogID`, `LogType`, `PortfolioID`, `BuyerID`, `LogDate`, `LogStatus`, `Amount`) VALUES
	(1, 'Buy', 1, 2, '2022-01-30 01:02:17', 1, NULL),
	(3, 'Buy', 1, 2, '2022-01-30 01:52:19', 1, NULL),
	(4, 'Buy', 1, 2, '2022-01-30 01:52:54', 1, NULL),
	(5, 'Buy', 1, 2, '2022-01-30 01:53:46', 1, NULL),
	(6, 'Buy', 1, 2, '2022-01-30 02:14:05', 1, NULL),
	(7, 'Buy', 1, 2, '2022-01-30 02:15:28', 1, NULL),
	(8, 'Buy', 1, 2, '2022-01-30 02:20:48', 1, 1),
	(9, 'Buy', 1, 2, '2022-01-30 02:37:48', 1, 1),
	(10, 'Buy', 1, 2, '2022-01-30 02:46:36', 1, 1),
	(11, 'Buy', 1, 2, '2022-01-30 02:47:14', 1, 1),
	(12, 'Buy', 1, 2, '2022-01-30 02:48:31', 1, 1);
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

-- tablo yapısı dökülüyor eva_exchange.shares_for_sale
CREATE TABLE IF NOT EXISTS `shares_for_sale` (
  `SaleID` int(11) NOT NULL AUTO_INCREMENT,
  `PortfolioID` int(11) DEFAULT NULL,
  `SalesShareAmount` float DEFAULT NULL,
  `SaleStatus` tinyint(1) DEFAULT NULL,
  `SaleDate` datetime DEFAULT NULL,
  PRIMARY KEY (`SaleID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='All shares for Sale';

-- eva_exchange.shares_for_sale: ~3 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `shares_for_sale` DISABLE KEYS */;
INSERT INTO `shares_for_sale` (`SaleID`, `PortfolioID`, `SalesShareAmount`, `SaleStatus`, `SaleDate`) VALUES
	(14, 1, 1, 1, '2022-01-30 02:54:55'),
	(15, 1, 2, 1, '2022-01-30 02:55:13'),
	(17, 2, 2, 1, '2022-01-30 02:59:29');
/*!40000 ALTER TABLE `shares_for_sale` ENABLE KEYS */;

-- tablo yapısı dökülüyor eva_exchange.users
CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) DEFAULT NULL,
  `UserSurname` varchar(50) DEFAULT NULL,
  `UserPhone` varchar(20) DEFAULT NULL,
  `UserEmail` varchar(100) NOT NULL,
  `UserPassword` varchar(120) NOT NULL,
  `UserTotalBalance` float DEFAULT NULL,
  `UserStatus` tinyint(1) DEFAULT 1,
  `RegisteredDate` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- eva_exchange.users: ~5 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`UserID`, `UserName`, `UserSurname`, `UserPhone`, `UserEmail`, `UserPassword`, `UserTotalBalance`, `UserStatus`, `RegisteredDate`) VALUES
	(1, 'İhsan', 'K.', '0 (535) 379 51 60', 'kiyici.ihsan@gmail.com', '202CB962AC59075B964B07152D234B70', 2000, 1, '2022-01-23 13:33:08'),
	(2, 'Ahmet', 'A.', '0 (536) 333 33 33', 'mrahmet@gmail.com', '202CB962AC59075B964B07152D234B70', 8500, 1, '2022-01-23 13:33:08'),
	(3, 'Mehmet', 'M.', '0 (539) 444 44 44', 'mrmehmet@gmail.com', '202CB962AC59075B964B07152D234B70', 10000, 1, '2022-01-23 13:33:08'),
	(4, 'Deniz', 'D.', '0 (539) 555 55 55', 'mrsdeniz@gmail.com', '202CB962AC59075B964B07152D234B70', 1500, 1, '2022-01-23 13:33:08'),
	(5, 'Batu', 'B.', '0 (532) 666 66 66', 'mrbatu@gmail.com', '202CB962AC59075B964B07152D234B70', 6000, 1, '2022-01-23 13:33:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- tablo yapısı dökülüyor eva_exchange.user_portfolios
CREATE TABLE IF NOT EXISTS `user_portfolios` (
  `PortfolioID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` int(11) DEFAULT NULL,
  `Currency` varchar(3) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `Price` float DEFAULT NULL,
  `PriceUpdateDate` datetime DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `BlockedAmount` int(11) DEFAULT NULL,
  PRIMARY KEY (`PortfolioID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='Trader''s shares in this table.';

-- eva_exchange.user_portfolios: ~4 rows (yaklaşık) tablosu için veriler indiriliyor
/*!40000 ALTER TABLE `user_portfolios` DISABLE KEYS */;
INSERT INTO `user_portfolios` (`PortfolioID`, `UserID`, `Currency`, `Amount`, `Price`, `PriceUpdateDate`, `Status`, `BlockedAmount`) VALUES
	(1, 1, 'AUX', 3, 1111, '2022-01-30 13:21:00', 1, 3),
	(2, 2, 'ABC', 20, 123, '2022-01-29 17:48:46', 1, 2),
	(3, 3, 'ASR', 5, 123, '2022-01-29 17:48:46', 1, 0),
	(4, 1, 'CBR', 25, 123, '2022-01-29 17:48:46', 1, 0);
/*!40000 ALTER TABLE `user_portfolios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
