﻿using System;

namespace eva
{
    public class BuyModel : Global
    {
        public int SaleID { get; set; } = 0;
        //public int PortfolioID { get; set; }
        public int RequesterUserID { get; set; } = 0;
        public int RequestAmount { get; set; } = 0;
    }
}
