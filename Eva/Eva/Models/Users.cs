﻿using System;

namespace eva
{
    public class Users: Global
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserSurname { get; set; }
        public string UserPhone { get; set; }
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public float UserTotalBalance { get; set; } = 0;
        public DateTime RegisteredDate { get; set; }
        public int UserStatus { get; set; }
    }
}
