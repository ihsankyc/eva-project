using System;

namespace eva
{
    public class UpdatePriceModel : Global
    {
        public int PortfolioID { get; set; } = 0;
        public float NewPrice { get; set; } = 0;
    }
}
