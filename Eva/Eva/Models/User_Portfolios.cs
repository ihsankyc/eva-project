﻿using System;

namespace eva
{
    public class User_Portfolios: Global
    {
        public int PortfolioID { get; set; }
        public int UserID { get; set; }
        public int BlockedAmount { get; set; } = 0;
        public string Currency { get; set; } = "";
        public float Amount { get; set; } = 0;
        public float Price { get; set; } = 0;
        public string UserName { get; set; } = "";
        public string UserSurname { get; set; } = "";
        public DateTime PriceUpdateDate { get; set; }
    }
}
