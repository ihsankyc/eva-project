﻿using System;

namespace eva
{
    public class Logs: Global
    {
        public int LogID { get; set; }
        public int PortfolioID { get; set; }
        public int BuyerID { get; set; }
        public int SellerID { get; set; }
        public string LogType { get; set; }
        public DateTime LogDate { get; set; }
        public int LogStatus { get; set; }
    }
}
