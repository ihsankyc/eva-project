﻿using System;

namespace eva
{
    public class Shares_For_Sale : Global
    {
        public int ShareID { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserSurname { get; set; }
        public string Currency { get; set; }
        public float Price { get; set; }
        public int SalesShareAmount { get; set; }
        public DateTime SaleDate { get; set; }
        //public int SaleStatus { get; set; }
    }
}
