﻿using System;

namespace eva
{
    public class SellModel : Global
    {
        public int PortfolioID { get; set; } = 0;
        public int RequestUserID { get; set; } = 0;
        public int AmountForSale { get; set; } = 0;
    }
}
