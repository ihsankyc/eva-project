﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using eva.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace eva.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class Buy : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public string ipAddres = "";
        public Buy(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        public string BuyNow([FromBody] BuyModel sharesForSales)
        {
            if (sharesForSales.AuthCode != Constants.AuthCode) return ApiHelper.ResponseMessage("ERROR", "401 Unauthorized");

            if (sharesForSales.SaleID!=0 && sharesForSales.RequesterUserID!=0 && sharesForSales.RequestAmount != 0)
            {
                ipAddres = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            
                string sqlDataSource = _configuration.GetConnectionString("EvaDbCon");
            
                int PortfolioID=BuyHelper.ExistStatusForSale(sqlDataSource, sharesForSales.SaleID,sharesForSales.RequestAmount);//Check Share for sale
                if (PortfolioID != 0)//if share is exist and amount enough.
                {
                    float TotalPriceForShare = BuyHelper.GetTotalPriceForBuy(sqlDataSource, PortfolioID, sharesForSales.RequestAmount); //Get TotalPrice

                    bool BalanceCheck = BuyHelper.BalanceCheckForUser(sqlDataSource, sharesForSales.RequesterUserID, TotalPriceForShare);

                    if (BalanceCheck)
                    {
                        bool buyShare = BuyHelper.BuyShare(sqlDataSource, PortfolioID, sharesForSales.RequesterUserID, sharesForSales.RequestAmount, 1);
                        if (buyShare)
                        {
                            List<User_Portfolios> SellerPortfolioDatas = BuyHelper.GetSellerInfo(sqlDataSource, PortfolioID);

                            if (SellerPortfolioDatas.Count == 0) { return ApiHelper.ResponseMessage("ERROR", "Seller Not Found!"); } //SELLER CHECK AND GET ID INFO
                        
                            /** ------------------- **/
                            //Decrease the Total Money For Buyer
                            bool updateTotalBalanceForBuyer = BuyHelper.UpdateUserTotalBalance(sqlDataSource, sharesForSales.RequesterUserID, TotalPriceForShare, "Buy");
                            //Increase the Total Money For Seller
                            bool updateTotalBalanceForSeller = BuyHelper.UpdateUserTotalBalance(sqlDataSource, SellerPortfolioDatas[0].UserID, TotalPriceForShare, "Sell");
                            /** ------------------- **/

                            if(updateTotalBalanceForSeller && updateTotalBalanceForBuyer)
                            {
                                //UPDATE USER PORTFOLIO TABLE FOR NEW AMOUNT AND BLOCKED AMOUNT
                                bool PortfolioStatus=BuyHelper.UpdateUserPortfolio(sqlDataSource, PortfolioID, sharesForSales.RequestAmount);
                                bool ShareStatus = BuyHelper.UpdateShareForSale(sqlDataSource, sharesForSales.SaleID, sharesForSales.RequestAmount);
                                if(ShareStatus && PortfolioStatus) {
                                    bool CreatePortfolio = BuyHelper.CreatePortfolio(sqlDataSource, sharesForSales.RequesterUserID, SellerPortfolioDatas[0].Currency, SellerPortfolioDatas[0].Price, sharesForSales.RequestAmount);
                                    return ApiHelper.ResponseMessage("SUCCESSFUL", "Buy process is Successful"); 
                                }
                                else { return ApiHelper.ResponseMessage("ERROR", "Final Problem!"); }
                            }
                            else
                            {
                                return ApiHelper.ResponseMessage("ERROR", "Users Balance not updated. Please Try Again Later");
                            }

                        }
                        else
                        {
                            BuyHelper.BuyShare(sqlDataSource, PortfolioID, sharesForSales.RequesterUserID, sharesForSales.RequestAmount, 0); //if not accepted request. Create as LogStatus = 0
                            return ApiHelper.ResponseMessage("ERROR", "Buy Proccess not completed! Could not bought share.");
                        }

                    }
                    else
                    {
                        return ApiHelper.ResponseMessage("ERROR", "User Balance Not Enough For Buy!");
                    }

                }
                else //not exist
                {
                    return ApiHelper.ResponseMessage("ERROR", "Share not exist or amount is much! Please check your Request");
                }
            }
            else
            {
                return ApiHelper.ResponseMessage("ERROR", "Please check your Request. Parameter Error!");
            }

        }



    }
}
