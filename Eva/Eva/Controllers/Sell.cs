﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using eva.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace eva.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class Sell : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public string ipAddres = "";
        public Sell(IConfiguration configuration)
        {

            _configuration = configuration;
        }

        [HttpPost]
        public string SellNow([FromBody] SellModel SellInfo)
        {
            if (SellInfo.AuthCode != Constants.AuthCode) return ApiHelper.ResponseMessage("ERROR", "401 Unauthorized");

            if (SellInfo.PortfolioID != 0 && SellInfo.AmountForSale != 0 && SellInfo.RequestUserID!=0)
            {

                string sqlDataSource = _configuration.GetConnectionString("EvaDbCon");

                bool SellStatus = SellHelper.ExistCurrencyCheck(sqlDataSource, SellInfo.PortfolioID, SellInfo.RequestUserID, SellInfo.AmountForSale);
                if (SellStatus)
                {
                    bool CreateNewShare=SellHelper.CreateNewShare(sqlDataSource, SellInfo.PortfolioID, SellInfo.AmountForSale);
                    if (CreateNewShare)
                    {
                        SellHelper.UpdateBlockedAmount(sqlDataSource, SellInfo.PortfolioID, SellInfo.AmountForSale);//Blocked Amount updated
                        return ApiHelper.ResponseMessage("SUCCESSFUL", "Share create is Successful");
                    }
                    else
                    {
                        return ApiHelper.ResponseMessage("ERROR", "Share create error. Plese Try Again!");
                    }
                }
                else
                {
                    return ApiHelper.ResponseMessage("ERROR", "Amount not enough or Portfolio not found!");
                }
            }
            else
            {
                return ApiHelper.ResponseMessage("ERROR", "Please check your Request. Parameter Error!");
            }

        }

    }
}
