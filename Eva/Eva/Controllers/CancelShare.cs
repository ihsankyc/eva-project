﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using eva.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace eva.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class CancelShare : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public string ipAddres = "";
        public CancelShare(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        public string CancelNow([FromBody] CancelShareModel CancelShare)
        {
            //ipAddres = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            if (CancelShare.AuthCode != Constants.AuthCode) return ApiHelper.ResponseMessage("ERROR", "401 Unauthorized");
            if (CancelShare.SaleID == 0) { return ApiHelper.ResponseMessage("ERROR", "PARAMETER ERROR!");  }

            string sqlDataSource = _configuration.GetConnectionString("EvaDbCon");
            MySqlConnection sqlConnection = new(sqlDataSource);
            sqlConnection.Open();

            string sqlCommand = @"UPDATE shares_for_sale SET SaleStatus=0 WHERE SaleID = @SaleID and SaleStatus=1;";

            MySqlCommand cmdCountOpen = new(sqlCommand, sqlConnection);

            cmdCountOpen.Parameters.AddWithValue("@SaleID", CancelShare.SaleID);

            var recs = cmdCountOpen.ExecuteNonQuery(); //Execute Process

            if (recs == 1)  return ApiHelper.ResponseMessage("SUCCESSFUL", "Share Status Cancelled!");
            return ApiHelper.ResponseMessage("ERROR", "Share Status can not Cancelled!");
        }
    }
}
