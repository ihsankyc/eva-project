﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using eva.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace eva.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ForSales : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public string ipAddres = "";
        public ForSales(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        public string GetSalesShares([FromBody] Shares_For_Sale Shares)
        {
            if (Shares.AuthCode != Constants.AuthCode) return ApiHelper.ResponseMessage("ERROR", "401 Unauthorized");

            string sqlDataSource = _configuration.GetConnectionString("EvaDbCon");
            List<Shares_For_Sale> SharesAll = new List<Shares_For_Sale>();

            string sqlQuery = $"SELECT user_portfolios.PortfolioID AS ShareID,users.UserID,users.UserName," +
                " users.UserSurname,user_portfolios.Currency,user_portfolios.Price,shares_for_sale.SalesShareAmount," +
                " shares_for_sale.SaleDate,shares_for_sale.SaleStatus FROM shares_for_sale LEFT JOIN user_portfolios" +
                " ON shares_for_sale.PortfolioID = user_portfolios.PortfolioID" +
                " LEFT JOIN users ON users.UserID = user_portfolios.UserID"; //SQL Query For Shares Infos

            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();

                MySqlCommand cmdCountOpen = new MySqlCommand(sqlQuery, myCnn);
                var myReader = cmdCountOpen.ExecuteReader(); //Data reading...

                while (myReader.Read())
                {
                    SharesAll.Add(new Shares_For_Sale
                    {
                        ShareID = (int)myReader["ShareID"],
                        UserID = (int)myReader["UserID"],
                        UserName = myReader["UserName"].ToString(),
                        UserSurname = myReader["UserSurname"].ToString(),
                        Currency = myReader["Currency"].ToString(),
                        Price = (float)myReader["Price"],
                        //SalesShareAmount = (int)myReader["SalesShareAmount"],
                        SaleDate = (DateTime)myReader["SaleDate"],
                    });
                }
                myCnn.Close();
                if (SharesAll.Count > 0)
                    return ApiHelper.ResponseMessage("SUCCESSFUL", SharesAll);
                else
                    return ApiHelper.ResponseMessage("ERROR", "Datas can not readed.");
            }
        }

    }
}
