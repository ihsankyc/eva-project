﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using eva.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace eva.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class GetPortfolios : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public string ipAddres = "";
        public GetPortfolios(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        public string GetUserPortfolio([FromBody] GetPortfoliosModel Portfolio)
        {
            if (Portfolio.AuthCode != Constants.AuthCode) return ApiHelper.ResponseMessage("ERROR", "401 Unauthorized");

            string sqlDataSource = _configuration.GetConnectionString("EvaDbCon");
            List<User_Portfolios> ReturnPortolios = new List<User_Portfolios>();

            string sqlQuery = $"SELECT u_p.*,u.UserName,u.UserSurname FROM user_portfolios AS u_p INNER JOIN users u ON u_p.UserID=u.UserID"; //SQL Query For Shares Infos
            if (Portfolio.PortfolioID != 0) { sqlQuery += " WHERE u_p.PortfolioID = " + Portfolio.PortfolioID; }
            if (Portfolio.UserID != 0) { sqlQuery += " WHERE u_p.UserID = " + Portfolio.UserID; }

            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();

                MySqlCommand cmdCountOpen = new MySqlCommand(sqlQuery, myCnn);
                var myReader = cmdCountOpen.ExecuteReader(); //Reading...

                while (myReader.Read())
                {
                    ReturnPortolios.Add(new User_Portfolios
                    {
                        UserID = (int)myReader["UserID"],
                        PortfolioID = (int)myReader["PortfolioID"],
                        Currency = myReader["Currency"].ToString(),
                        UserName = myReader["UserName"].ToString(),
                        UserSurname = myReader["UserSurname"].ToString(),
                        Amount = (int)myReader["Amount"],
                        Price = (float)myReader["Price"],
                        BlockedAmount = (int)myReader["BlockedAmount"],
                        PriceUpdateDate = (DateTime)myReader["PriceUpdateDate"],
                    });
                }
                myCnn.Close();
                if (ReturnPortolios.Count > 0)
                    return ApiHelper.ResponseMessage("SUCCESSFUL", ReturnPortolios);
                else
                    return ApiHelper.ResponseMessage("ERROR", "Datas can not readed.");
            }
        }


    }
}
