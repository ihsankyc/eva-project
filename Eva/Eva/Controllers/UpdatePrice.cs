﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using eva.Helpers;
using System.Linq;
using System.Threading.Tasks;

namespace eva.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class UpdatePrice : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public string ipAddres = "";
        public UpdatePrice(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        public string UpdateNow([FromBody] UpdatePriceModel UpdateNewPrice)
        {
            if (UpdateNewPrice.AuthCode != Constants.AuthCode) return ApiHelper.ResponseMessage("ERROR", "401 Unauthorized");

            if (UpdateNewPrice.PortfolioID == 0 || UpdateNewPrice.NewPrice==0) { return ApiHelper.ResponseMessage("ERROR", "PARAMETER ERROR!");  }
            
            string requestDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");//get request date
            Console.WriteLine(requestDate);
            string sqlDataSource = _configuration.GetConnectionString("EvaDbCon");
            MySqlConnection sqlConnection = new(sqlDataSource);
            sqlConnection.Open();
            //if 1 hour has passed
            string sqlCommand = @"UPDATE user_portfolios SET Price=@NewPrice WHERE TIME_TO_SEC(TIMEDIFF( @requestDate,PriceUpdateDate)) > 3600 AND PortfolioID=1 AND Status=1;";

            MySqlCommand cmdCountOpen = new(sqlCommand, sqlConnection);

            cmdCountOpen.Parameters.AddWithValue("@PortfolioID", UpdateNewPrice.PortfolioID);
            cmdCountOpen.Parameters.AddWithValue("@NewPrice", (float)UpdateNewPrice.NewPrice);
            cmdCountOpen.Parameters.AddWithValue("@requestDate", requestDate);

            var recs = cmdCountOpen.ExecuteNonQuery(); //Execute Proccess

            if (recs == 1)  return ApiHelper.ResponseMessage("SUCCESSFUL", "Price updated successful!");
            return ApiHelper.ResponseMessage("ERROR", "Price can not updated! Should be 1 hour has passed from last update");
        }
    }
}
