﻿using System;
using System.Security.Cryptography;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Text;
using Nancy.Json;
using System.Collections.Generic;

namespace eva.Helpers
{
    public class BuyHelper
    {
        //CHECK STATUS FOR REQUEST SALE ID
        public static int ExistStatusForSale(string sqlDataSource, int SaleID, int amount)
        {
            int PortfolioID = 0;//default
            string countQueryProcess = @"SELECT PortfolioID FROM shares_for_sale"; //SQL Query
            countQueryProcess += " where SaleStatus=1 and SaleID=@SaleID and SalesShareAmount >= @Amount LIMIT 1;";
            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@SaleID", SaleID);
                cmdCountOpen.Parameters.AddWithValue("@Amount", amount);
                var myReader = cmdCountOpen.ExecuteReader(); //Start to read
                if (myReader.Read()) // if data exist
                {
                    PortfolioID = (int)myReader["PortfolioID"];
                }
                myCnn.Close();
            }
            return PortfolioID;
        }

        //CHECK BALANCE FOR REQUEST USER. ENOUGH OR NOT?
        public static bool BalanceCheckForUser(string sqlDataSource, int UserID, float TotalPrice)
        {
            bool status = false;
            string countQueryProcess = @"SELECT UserID FROM users"; //SQL Query
            countQueryProcess += " where UserStatus=1 and UserID = @UserID and UserTotalBalance >= @TotalPrice";
            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@UserID", UserID);
                cmdCountOpen.Parameters.AddWithValue("@TotalPrice", TotalPrice);
                var myReader = cmdCountOpen.ExecuteReader(); //Start to read
                if (myReader.Read()) // if data exist
                {
                    status = true;
                }
                myCnn.Close();
            }
            return status;
        }

        //AMOUNT * PRICE. CHECK ENOUGH OR NOT?
        public static float GetTotalPriceForBuy(string sqlDataSource, int PortfolioID,int RequestAmount)
        {
            float TotalPrice = 0;
            string countQueryProcess = @"SELECT Price FROM user_portfolios"; //SQL Query
            countQueryProcess += " where PortfolioID=@PortfolioID and Status=1";
            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@PortfolioID", PortfolioID);
                var myReader = cmdCountOpen.ExecuteReader(); //Start to read
                if (myReader.Read()) // if data exist
                {
                    TotalPrice = RequestAmount * (float)myReader["Price"];
                }
                myCnn.Close();
            }
            return TotalPrice;
        }

        //GET SELLER INFOS WE NEED IN "BUY" FUNCTION
        public static List<User_Portfolios> GetSellerInfo(string sqlDataSource, int PortfolioID)
        {
            List<User_Portfolios> PortfolioDatas = new List<User_Portfolios>();

            string countQueryProcess = @"SELECT UserID,Currency,Price FROM user_portfolios"; //SQL Query
            countQueryProcess += " where PortfolioID=@PortfolioID and Status=1 LIMIT 1";
            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@PortfolioID", PortfolioID);
                var myReader = cmdCountOpen.ExecuteReader(); //Start to read
                while (myReader.Read())
                {
                    PortfolioDatas.Add(new User_Portfolios
                    {
                        UserID = (int)myReader["UserID"],
                        Currency = myReader["Currency"].ToString(),
                        Price = (float)myReader["Price"],
                    });
                }
                myCnn.Close();
            }
            return PortfolioDatas;
        }

        //BUY THE SHARE!
        public static bool BuyShare(string sqlDataSource,int PortfolioID,int BuyerID,int Amount,int LogStatus)
        {
            try
            {
                bool status = false;
                string countQueryProcess = "INSERT INTO logs (LogType,PortfolioID,BuyerID,Amount,LogDate,LogStatus)";
                countQueryProcess += " VALUES (@LogType,@PortfolioID,@BuyerID,@Amount,@LogDate,@LogStatus);";
                using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
                {
                    myCnn.Open();
                    MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                    cmdCountOpen.Parameters.AddWithValue("@LogType", "Buy");
                    cmdCountOpen.Parameters.AddWithValue("@PortfolioID", PortfolioID);
                    cmdCountOpen.Parameters.AddWithValue("@BuyerID", BuyerID);
                    cmdCountOpen.Parameters.AddWithValue("@Amount", Amount);
                    cmdCountOpen.Parameters.AddWithValue("@LogDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                    cmdCountOpen.Parameters.AddWithValue("@LogStatus", LogStatus);
                    var Exec = cmdCountOpen.ExecuteNonQuery(); //Execute Proccess
                    if (Exec == 1) // if row created
                    {
                        //Process is successful
                        status = true;
                    }
                    myCnn.Close();
                }
                return status;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            
        }

        //UPDATE USER TOTAL BALANCE IN PROCESS (BUY OR SELL)
        public static bool UpdateUserTotalBalance(string sqlDataSource, int UserID, float Balance, string Status="")
        {
            bool status = false;
            string countQueryProcess = "";
            //if request is increase the Total balance
            if (Status == "Sell")
            {
                countQueryProcess = "UPDATE users SET UserTotalBalance = UserTotalBalance + @Balance ";
            }
            else//if request is decrease the Total balance
            {
                countQueryProcess = "UPDATE users SET UserTotalBalance = UserTotalBalance - @Balance ";
            }

            countQueryProcess += " WHERE UserID=@UserID"; //WHERE METHOD
            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@UserID", UserID);
                cmdCountOpen.Parameters.AddWithValue("@Balance", Balance);
                var Exec = cmdCountOpen.ExecuteNonQuery(); //Execute işlemi
                if (Exec == 1) // if row created
                {
                    //Process is successful
                    status = true;
                }
                myCnn.Close();
            }
            return status;
        }

        //UPDATE USER PORTFOLIO WITH NEW INFOS
        public static bool UpdateUserPortfolio(string sqlDataSource, int PortfolioID, int RequestAmount)
        {
            bool status = false;
            string countQueryProcess = "";
            //increase the Total Amount
            countQueryProcess = "UPDATE user_portfolios SET Amount = Amount - @RequestAmount , BlockedAmount=BlockedAmount - @RequestAmount ";
            countQueryProcess += " WHERE PortfolioID=@PortfolioID"; //WHERE METHOD

            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@PortfolioID", PortfolioID);
                cmdCountOpen.Parameters.AddWithValue("@RequestAmount", RequestAmount);
                var Exec = cmdCountOpen.ExecuteNonQuery(); //Execute işlemi
                if (Exec == 1) // if row created
                {
                    //Process is successful
                    status = true;
                }
                myCnn.Close();
            }
            return status;
        }

        //UPDATE SHARE AMOUNT FOR NEXT REQUEST
        public static bool UpdateShareForSale(string sqlDataSource, int SaleID, int RequestAmount)
        {
            bool status = false;
            string countQueryProcess = "";
            //increase the Total Amount
            countQueryProcess = "UPDATE shares_for_sale SET SalesShareAmount = SalesShareAmount - @RequestAmount";
            countQueryProcess += " WHERE SaleID=@SaleID"; //WHERE METHOD

            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@SaleID", SaleID);
                cmdCountOpen.Parameters.AddWithValue("@RequestAmount", RequestAmount);
                var Exec = cmdCountOpen.ExecuteNonQuery(); //Execute işlemi
                if (Exec == 1) // if row created
                {
                    //Process is successful
                    status = true;
                }
                myCnn.Close();
            }
            return status;
        }

        //CREATE NEW PORTFOLIO FOR NEW BUYER OR EXIST BUYER
        public static bool CreatePortfolio(string sqlDataSource, int UserID, string Currency, float Price, int Amount)
        {
            try
            {
                bool status = false;
                string countQueryProcess = "INSERT INTO user_portfolios (UserID,Currency,Price,Amount,PriceUpdateDate,Status)";
                countQueryProcess += " VALUES (@UserID,@Currency,@Price,@Amount,@PriceUpdateDate,@Status);";
                using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
                {
                    myCnn.Open();
                    MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                    cmdCountOpen.Parameters.AddWithValue("@UserID", UserID);
                    cmdCountOpen.Parameters.AddWithValue("@Currency", Currency);
                    cmdCountOpen.Parameters.AddWithValue("@Price", Price);
                    cmdCountOpen.Parameters.AddWithValue("@Amount", Amount);
                    cmdCountOpen.Parameters.AddWithValue("@PriceUpdateDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                    cmdCountOpen.Parameters.AddWithValue("@Status", 1);
                    var Exec = cmdCountOpen.ExecuteNonQuery(); //Execute Proccess
                    if (Exec == 1) // if row created
                    {
                        //Process is successful
                        status = true;
                    }
                    myCnn.Close();
                }
                return status;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

        }

    }
}
