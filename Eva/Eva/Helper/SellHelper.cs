﻿using System;
using System.Security.Cryptography;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Text;
using Nancy.Json;
using System.Collections.Generic;

namespace eva.Helpers
{
    public class SellHelper
    {
        //CHECK STATUS FOR REQUEST PORTFOLIO ID
        public static bool ExistCurrencyCheck(string sqlDataSource, int PortfolioID, int RequestUserID, int SalesShareAmount)
        {
            bool status = false;
            //IF BLOCKED AMOUNT + REQUEST AMOUNT LOWER THEN TOTAL AMOUNT. THEN ALLOW.
            string countQueryProcess = @"SELECT PortfolioID FROM user_portfolios"; //SQL Query
            countQueryProcess += " where Status=1 and PortfolioID=@PortfolioID and UserID=@RequestUserID and Amount >= @amount + BlockedAmount";
            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@PortfolioID", PortfolioID);
                cmdCountOpen.Parameters.AddWithValue("@RequestUserID", RequestUserID);
                cmdCountOpen.Parameters.AddWithValue("@amount", SalesShareAmount);
                var myReader = cmdCountOpen.ExecuteReader(); //Start to read
                if (myReader.Read()) // if data exist
                {
                    status = true;
                }
                myCnn.Close();
            }
            return status;
        }

        //CREATE NEW SHARE FOR NEW SELLER
        public static bool CreateNewShare(string sqlDataSource, int PortfolioID, int SalesShareAmount)
        {
            try
            {
                bool status = false;
                string countQueryProcess = "INSERT INTO shares_for_sale (PortfolioID,SalesShareAmount,SaleDate,SaleStatus)";
                countQueryProcess += " VALUES (@PortfolioID,@SalesShareAmount,@SaleDate,@SaleStatus);";
                using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
                {
                    myCnn.Open();
                    MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                    cmdCountOpen.Parameters.AddWithValue("@PortfolioID", PortfolioID);
                    cmdCountOpen.Parameters.AddWithValue("@SalesShareAmount", SalesShareAmount);
                    cmdCountOpen.Parameters.AddWithValue("@SaleDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                    cmdCountOpen.Parameters.AddWithValue("@SaleStatus", 1);
                    var Exec = cmdCountOpen.ExecuteNonQuery(); //Execute Proccess
                    if (Exec == 1) // if row created
                    {
                        //Process is successful
                        status = true;
                    }
                    myCnn.Close();
                }
                return status;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

        }

        //UPDATE BLOCKED AMOUNT FOR SELLER
        public static bool UpdateBlockedAmount(string sqlDataSource, int PortfolioID, int SalesShareAmount)
        {
            bool status = false;
            string countQueryProcess = "";
            //increase the Total Amount
            countQueryProcess = "UPDATE user_portfolios SET BlockedAmount = BlockedAmount + @SalesShareAmount";
            countQueryProcess += " WHERE PortfolioID=@PortfolioID"; //WHERE METHOD

            using (MySqlConnection myCnn = new MySqlConnection(sqlDataSource))
            {
                myCnn.Open();
                MySqlCommand cmdCountOpen = new MySqlCommand(countQueryProcess, myCnn);
                cmdCountOpen.Parameters.AddWithValue("@PortfolioID", PortfolioID);
                cmdCountOpen.Parameters.AddWithValue("@SalesShareAmount", SalesShareAmount);
                var Exec = cmdCountOpen.ExecuteNonQuery(); //Execute işlemi
                if (Exec == 1) // if row created
                {
                    //Process is successful
                    status = true;
                }
                myCnn.Close();
            }
            return status;
        }

    }
}
