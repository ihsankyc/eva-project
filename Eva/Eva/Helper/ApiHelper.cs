﻿using System;
using System.Security.Cryptography;
using System.Text;
using Nancy.Json;

namespace eva.Helpers
{
    public class ApiHelper
    {
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            
            //metnin boyutundan hash hesaplar
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //hesapladıktan sonra hashi alır
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //her baytı 2 hexadecimal hane olarak değiştirir
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public static string ResponseMessage(string _ResponseMsg, dynamic _Result) 
        { 
            var json =""; 
 
            var _ResponseJson = new 
            { 
                responseMsg = _ResponseMsg, 
                result = _Result, 
 
            }; 
            json = new JavaScriptSerializer().Serialize(_ResponseJson); 
 
            return json; 
        }

        public static bool IPCheck(string GetIP)
        {

            String[] strings = { "127.0.0.1" };

            int idx = Array.IndexOf(strings, GetIP);
            if (idx != -1)
            {
                return true;
            }
            return false;
        }

    }
}
